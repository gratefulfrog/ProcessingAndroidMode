# ProcessingAndroidMode
Tests using Processing.org's Adnroid Mode and Ketai library

Lots of exercises from the book *Rapid Android Development* which have been adapted to actually 
work on my Nexus and Yezz devices. 

This is running on Processing 3.3 and the Android SDK API 6.0 (23).


# Issues and Workarounds
As I worked through the book, I documented my problems and solutions (when found) on [this page](https://gitlab.com/gratefulfrog/Car/wikis/Processing-Android-mode-and-Ketai-Library)
